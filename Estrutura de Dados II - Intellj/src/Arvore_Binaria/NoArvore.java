package Arvore_Binaria;

public class NoArvore {
    int dado;
    NoArvore esquerdo;
    NoArvore direito;
    NoArvore anterior;

    public NoArvore() {

    }

    public NoArvore(int dado, NoArvore raiz) {
        this.dado=dado;
        this.anterior=raiz;
    }

}
