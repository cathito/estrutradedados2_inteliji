package Arvore_Binaria;

public class Arvore {

    NoArvore raiz;
// ----------------------------------//

    public boolean arvoreEstaVazia() {
        try{
            int a=raiz.dado;
        }catch (NullPointerException e){
            return true;
        }
        return false;
    }
// ==============================================================================================================//

    public void add(int dado) {
        add(raiz,raiz, dado);
        System.out.println("Valor: " + dado + ", foi Adicionando a Arvore!");
    }

    private NoArvore add(NoArvore Raiz, NoArvore novaSubRaiz, int dado) {

        if (novaSubRaiz == null) {
            novaSubRaiz = new NoArvore(dado,Raiz);

            if (raiz == null)
                raiz = novaSubRaiz;

        } else if (dado < novaSubRaiz.dado) {
            novaSubRaiz.esquerdo = add(novaSubRaiz, novaSubRaiz.esquerdo, dado);

        } else {
            novaSubRaiz.direito = add(novaSubRaiz, novaSubRaiz.direito, dado);
        }
        return novaSubRaiz;
    }
// ==============================================================================================================//

    public void buscarValor(int valor) {
        if( buscarValor(raiz, valor) != null) {
            System.out.println("Valor " + valor + ", Encontrado!");
        }else {
            System.out.println("Valor " + valor + ", Não Encontrado!");
        }
    }

    public NoArvore buscarValor(NoArvore subRaiz, int valor) {

        if (subRaiz == null) {
            return null;

        } else {
            if (valor < subRaiz.dado) {
                return buscarValor(subRaiz.esquerdo, valor);

            } else if (valor > subRaiz.dado){
                return buscarValor(subRaiz.direito, valor);
            }
            return subRaiz;
        }
    }

// ==============================================================================================================//
// ==============================================================================================================//


    public void remover(int valor) {
        NoArvore noEncontrado = buscarValor(raiz,valor);
        if(noEncontrado == null) {
            System.out.println("Valor " + valor + ", Não Encontrado!");
        }else {
            remover(noEncontrado);
            System.out.println("O valor " + valor + " foi removido!");
        }
    }

    private NoArvore remover(NoArvore atual) {

        if (atual.esquerdo == null && atual.direito == null) { //ok
            if(atual.anterior != null) {
                AnteriorApontePara(atual, null);
            }else
                raiz=new NoArvore();

        }else if (atual.esquerdo == null) {
            NoArvore salveAnteriorAtual = AnteriorApontePara(atual, atual.direito);

            atual.direito.anterior = salveAnteriorAtual;
            atual.direito =null;


        }else if (atual.direito == null) {
            NoArvore salveAnteriorAtual = AnteriorApontePara(atual, atual.esquerdo);

            atual.esquerdo.anterior = salveAnteriorAtual;
            atual.esquerdo = null;
        }

// ==============================================  possui filhos  ==============================================//
        else{
            if(atual.esquerdo.direito == null){

                NoArvore salveRemovido = atual;
                
                atual = atual.esquerdo;
                atual.direito = salveRemovido.direito;
                salveRemovido.direito.anterior = atual;

                if(salveRemovido.anterior != null) {
                    atual.anterior = AnteriorApontePara(salveRemovido, atual);
                }else{
                    atual.anterior = salveRemovido.anterior;
                }

            }else{ // ta ok perfeito
                atual.dado=pegarFolhaEsquerda(atual.esquerdo.direito);
            }
        }
        return atual;
    }




    private NoArvore AnteriorApontePara(NoArvore atual, NoArvore novoApontamento){
        if(atual.anterior.dado>atual.dado){
            atual.anterior.esquerdo = novoApontamento;
        }else{
            atual.anterior.direito = novoApontamento;
        }
        return atual.anterior;
    }

















//----------------------------------------------------------------------------//
    public int pegarFolhaEsquerda(NoArvore atual) { // Parametro = atual.direito

        if (atual.esquerdo != null) {
            return pegarFolhaEsquerda(atual.esquerdo);

        }else if (atual.direito != null ) {
            return pegarFolhaEsquerda(atual.direito);
        }

        if (atual.anterior.dado >atual.dado)
            atual.anterior.esquerdo=null;
        else
            atual.anterior.direito=null;

        return atual.dado;
    }


// ==============================================================================================================//
// ==============================================================================================================//


    public void emOrdem() { // E/R/D
        imprimiOrdenado(raiz);
    }

    private void imprimiOrdenado(NoArvore no) {
        if (no != null) {
            imprimiOrdenado(no.esquerdo);
            System.out.print(no.dado + ", ");
            imprimiOrdenado(no.direito);
        }
    }

    // -----------------------------------------------------//
    public void emOrdeminverso() {
        imprimiOrdenadoinverso(raiz);
    }

    private void imprimiOrdenadoinverso(NoArvore no) {
        if (no != null) {
            imprimiOrdenado(no.direito);
            System.out.print(no.dado + ", ");
            imprimiOrdenado(no.esquerdo);
        }
    }

// ==============================================================================================================//
// ==============================================================================================================//

    public void Altura() {
        int point = 0;
        int altramMaxima = 0;

        System.out.println("\nNa linguagem de maquina a altura da arvore é: " + (Altura(raiz, point, altramMaxima) - 1));
        System.out.println("Na linguagem humana a altura da arvore é: " + (Altura(raiz, point, altramMaxima)) + "\n");
    }

    private int Altura(NoArvore subRaiz, int point, int altramMaxima) {
        point++;

        if (subRaiz.esquerdo != null) {
            return Altura(subRaiz.esquerdo, point, altramMaxima);
        } else {
            if (subRaiz.direito != null) {
                return Altura(subRaiz.direito, point, altramMaxima);
            }
        }

        if (point > altramMaxima) {
            altramMaxima = point;
        }

        return altramMaxima;
    }
// --------------------------------------------------------------------------//

    public void numeroFolhas() {
        int point = 0;
        int altramMaxima = 0;
        System.out.println(
                "A arvore possui um limite maximo de " + ((int) Math.pow(2, Altura(raiz, point, altramMaxima) - 1)) + ", folhas!");
    }
// --------------------------------------------------------------------------//

    public void totalElementos() {
        int point = 0;
        int altramMaxima = 0;
        System.out.println("A arvore possui um limite maximo de " + (((int) Math.pow(2, Altura(raiz, point, altramMaxima))) - 1)
                + ", elementos!");
    }

// ==============================================================================================================//
// ==============================================================================================================//

    public void PercorrerEmLargura() {
        if(arvoreEstaVazia()){
            System.out.println("A Arvore esta Vazia");
        }else {
            FilaSE fila = new FilaSE();
            fila.Adicionar(raiz);
            PercorrerEmLargura(fila, fila.getInicio().dado);
        }
    }

    private void PercorrerEmLargura(FilaSE fila, NoArvore noAtual) {
        System.out.println(noAtual.dado);

        if (noAtual.esquerdo != null) {
            fila.Adicionar(noAtual.esquerdo);
        }
        if (noAtual.direito != null) {
            fila.Adicionar(noAtual.direito);
        }
        fila.Remover();

        if (fila.getInicio() != null) {
            PercorrerEmLargura(fila, fila.getInicio().dado);
        }

    }

// ==============================================================================================================//


}
